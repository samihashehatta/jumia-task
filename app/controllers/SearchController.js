const BaseController = require('./BaseController');
const SearchService = require('../services/SearchService');

module.exports = class SearchController extends BaseController {

    /**
     * @param {Express} server
     * @param {SearchService} SearchService 
     */
    constructor(server, searchService) {
        super(server);
        this.SearchService = searchService || new SearchService();
    }
    

    /**
     * Register Controller
     */
    init() {
        this.server.get('/api/v1/search',this.validation, (req, res, next) => this.searchAction(req, res, next));
    }
    
    /**
     * @api {get} /api/v1/search Search for books
     * @apiVersion 1.0.0
     * @apiName Search V1
     * @apiGroup Search
     * @ApiDescription V1 Search for books in goodreads API
     * @apiUse Response100
     */
    /**
     * Search Action
     * @param {Object} req 
     * @param {Object} res 
     * @param {function} next 
     */
    validation(req,res,next){
        let sortingMust = ['most rated','least rated']
        let errorMsg = ''
        if(!req.query.q)errorMsg ='you must give search query '
        if(req.query.sort && !sortingMust.includes(req.query.sort))errorMsg ='sorting value is worng'
        if(Math.sign(req.query.year) == -1)errorMsg ='year must be postive number'
        if(req.query.year&&req.query.year.length < 4)errorMsg ='year must be 4 digits'
        if(errorMsg) res.status(400).json(super.sendResponse('BACKEND_ERROR', errorMsg));
        else next()

    }
    async searchAction(req, res, next) {
        // so i didn't know what kind of filteration and sorting you wanted so i added sorting by rating and filter by year 
        let searchParams ={
            q:req.query.q,
            sort:req.query.sort,
            year:req.query.year,
        }
        try {
            let result = await  this.SearchService.search(searchParams);
            res.status(200).json(super.sendResponse('SUCCESS', result));
        } catch (e) {
            res.status(500).json(super.sendResponse('BACKEND_ERROR', e.message));
        }
    }
}