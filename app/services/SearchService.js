const fetch = require('node-fetch');
const config = require('./../../config/config');
const parser = require('xml2json');

module.exports = class SearchService {

  constructor(configs) {
    this.configs = configs || config;
  }

  search(searchParams) {
    return new Promise((resolve, reject) => {
      //so i searched the goodreads docs but i didn't find any sorting queries or apis 
        let baseUrl = `${config.goodreads.base_url}${config.goodreads.search_resource}`
        fetch( baseUrl+searchParams.q+ '&key=bZFY4Rc5TZpBEc89fv7XKA', 
            { 
              method: 'GET', 
              headers: { 'Content-Type': 'application/json' }
            }
        ).then(res => res.text()
        ).then(xml => { 
          let json = JSON.parse(parser.toJson(xml));
          let result = json.GoodreadsResponse.search.results.work
          if(searchParams.sort) result = this.sorting(searchParams.sort,result)
          if(searchParams.year) result= this.filterByYear(searchParams.year,result)
          return resolve(result)
        }).catch(e => {
          return reject(e)
        });

    });
  }
  sorting(sortParam,result){
    if(sortParam == 'most rated') return this.sortingByMostRated(result)
    else if (sortParam == 'least rated')return this.sortingByLeastRated(result)
  }
  sortingByLeastRated(result){
    let sorted =  result.sort((resultA,resultB)=>{
      return parseFloat(resultA.average_rating)-parseFloat(resultB.average_rating)
    })
    return sorted
  }
  sortingByMostRated(result){
    let sorted =  result.sort((resultA,resultB)=>{
      return parseFloat(resultB.average_rating)-parseFloat(resultA.average_rating)
    })
    return sorted
  }
  filterByYear(filterParam,result){
    return result.filter(book=>book.original_publication_year.$t == filterParam)
  }
}